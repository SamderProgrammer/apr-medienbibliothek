public class VHS extends Medium{

    private int spieldauer;
    private String qualität;
    public VHS(String titel, double wert, String standort, int spieldauer, String qualität) {
        super(titel, wert, standort);
        this.qualität=qualität;
        this.spieldauer=spieldauer;
    }

    public int getSpieldauer() {
        return spieldauer;
    }

    public String getQualität() {
        return qualität;
    }

    public void setSpieldauer(int spieldauer) {
        this.spieldauer = spieldauer;
    }

    public void setQualität(String qualität) {
        this.qualität = qualität;
    }

    public void anzeigen(){
        super.anzeigen();
        System.out.println("VHS: Spieldauer -> " + this.getSpieldauer() + " Qualität -> " + this.getQualität());
    }
}
