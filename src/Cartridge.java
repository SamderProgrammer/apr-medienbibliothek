public class Cartridge extends Medium {
    private boolean vollstädnigesSet;
    private String entwicklerstudio;
    private Konsolentyp konsole;


    public Cartridge(String titel, double wert, String standort, boolean vollstädnigesSet, String entwicklerstudio, Konsolentyp konsole) {
        super(titel, wert, standort);
        this.vollstädnigesSet=vollstädnigesSet;
        this.entwicklerstudio=entwicklerstudio;
        this.konsole = konsole;
    }

    public boolean isVollstädnigesSet() {
        return vollstädnigesSet;
    }

    public String getEntwicklerstudio() {
        return entwicklerstudio;
    }

    public Konsolentyp getKonsole() {
        return konsole;
    }

    public void setVollstädnigesSet(boolean vollstädnigesSet) {
        this.vollstädnigesSet = vollstädnigesSet;
    }

    public void setEntwicklerstudio(String entwicklerstudio) {
        this.entwicklerstudio = entwicklerstudio;
    }

    public void setKonsole(Konsolentyp konsole) {
        this.konsole = konsole;
    }

    public void anzeigen(){
        super.anzeigen();
        System.out.println("VHS: Entwicklungsstudio -> " + this.getEntwicklerstudio() + " VollständigesSet -> " + this.isVollstädnigesSet() + " Konsolentyp -> " + this.getKonsole());
    }
}
